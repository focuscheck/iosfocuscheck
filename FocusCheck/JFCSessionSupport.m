//
//  JFCSessionSupport.m
//  FocusCheck
//
//  Created by Josh DeWald on 2/2/14.
//
//

#import "JFCSessionSupport.h"
#import "FocusCheckSession.h"

@implementation JFCSessionSupport

+(void) cleanupSessions:(NSManagedObjectContext*)ctx;
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FocusCheckSession"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstDate" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
   //    NSTimeInterval nowInterval = [now timeIntervalSince1970];
 //   NSPredicate *pred = [NSPredicate predicateWithFormat:@"nextDate + (length * 60) < %d", nowInterval];
   // request.predicate = pred;
    NSDate *now  = [NSDate date];
    NSError *err;
    NSArray *expired = [ctx executeFetchRequest:request error:&err];
    NSInteger deleteCount = 0;
    NSInteger moveCount = 0;
    

    if (! expired) {
        NSLog(@"Error fetching: %@", err);
    } else {
        for (FocusCheckSession *s in expired) {
            [JFCSessionSupport convertToNewRecurrence:s];
            NSDate *nextDate = [NSDate dateWithTimeIntervalSince1970:s.nextDate];
            NSDate *endDate = [nextDate dateByAddingTimeInterval:(s.length * 60)];
            if ([endDate compare:now] >= 0) {
                // skipping
                continue;
            }
            if (s.recurring == 0) {
                [ctx deleteObject:s];
                deleteCount++;
            } else {
                NSDate* newNext = [JFCSessionSupport findNextDate:s];
                if (newNext == nil) {
                    continue;
                }
                if ([newNext compare:nextDate] != 0){
                    s.nextDate = [newNext timeIntervalSince1970];

                    moveCount++;
                }
               
            }
         //   NSLog(@"Date + interval is: %i", (s.firstDate + s.length));
        }
        if (![ctx save:&err]) {
            NSLog(@"Error during delete: %@", err);
        } else {
            NSLog(@"Cleaned up %i sessions and moved %i sessions", deleteCount, moveCount);
            
        }
       
    }
    NSLog(@"Done cleaning up");
}

+(void) convertToNewRecurrence:(FocusCheckSession*)session
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    if (session.recurring > 0) {
        if (session.recurring == NSWeekCalendarUnit) {
            NSDate* nextDate = [NSDate dateWithTimeIntervalSince1970:session.firstDate];
            NSDateComponents *dateComponents = [calendar components:NSWeekdayCalendarUnit fromDate:nextDate];
            session.recurring |= (1UL << [dateComponents weekday]);
            NSLog(@"Converted session with initial fire of %@ to be weekly on %i", nextDate, [dateComponents weekday]);
        } else if (session.recurring == NSDayCalendarUnit) { // every day of the week
            // note that even though one of the days would run into this, we use week unit + each day
            session.recurring = NSWeekCalendarUnit;
            session.recurring |= RECUR_ANY;
            NSLog(@"Converted session with initial fire of %@ to every day of the week, weekly", [NSDate dateWithTimeIntervalSince1970:session.firstDate]);
        }
    }
}

/**
  * Identify the next time this session will occur, applying the recurrence
 */
+(NSDate*)findNextDate:(FocusCheckSession *)session
{
    return [JFCSessionSupport findNextDate:session greaterThan:[NSDate date]];
}

+(NSDate*)findNextDate:(FocusCheckSession *)session greaterThan:(NSDate*)now
{
    //NSDate *now  = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [NSDate dateWithTimeIntervalSince1970:session.nextDate];
    NSDate *endDate = [nextDate dateByAddingTimeInterval:(session.length * 60)];
    
    if ([endDate compare:now] > 0) {
        return nextDate;
    }
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    NSDateComponents *dateComponents = [calendar components:(NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit)  fromDate:nextDate];

    // have we specified specific day recurrences?
    
    
    NSDate *newNext = nextDate;
    if (session.recurring & RECUR_ANY) {
        int day = [dateComponents weekday]; // 1 = sunday
        day++;
        while ([newNext compare:now] <= 0) {
            
            // we have, so locate the next day of the week we are scheduled for
            // and use the differential to add at the end
           

            while (day < 8) {
                if (session.recurring & (1UL << day)) {
                    [dateComponents setWeekday:day];
                    newNext = [calendar dateFromComponents:dateComponents];
                    break;
                }
                day++;
            }
            
            if (day == 8) {
                [dateComponents setWeek:[dateComponents week] + 1];
                day = 1;
            }
           
        }
    } else {
        
        switch (session.recurring){
            case 0:
                return nil;
            case 1:
            case NSWeekCalendarUnit: {
                
                [components setWeek:1];
            }
                break;
                
            case NSDayCalendarUnit:
                [components setDay:1];
                break;
            default:
                NSLog(@"Unknown recurrence type: %i", session.recurring);
                return nextDate;
                
        }
        
        
        while ([newNext compare:now] <= 0) {
            newNext = [calendar dateByAddingComponents:components toDate:newNext options:0];
            NSLog(@"Moved session from %@ to %@", nextDate, newNext);
            
        }
    }

    return newNext;
}

@end
