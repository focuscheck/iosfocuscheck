//
//  JFCSessionSupport.h
//  FocusCheck
//
//  Created by Josh DeWald on 2/2/14.
//
//

#import <Foundation/Foundation.h>
#import "FocusCheckSession.h"

@interface JFCSessionSupport : NSObject
+(void)cleanupSessions:(NSManagedObjectContext*)ctx;
+(NSDate*)findNextDate:(FocusCheckSession*)session;
+(NSDate*)findNextDate:(FocusCheckSession*)session greaterThan:(NSDate*)endDate;
@end
