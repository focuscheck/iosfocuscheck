//
//  JFCSecondViewController.m
//  FocusCheck
//
//  Created by Joshua DeWald on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JFCChartsViewController.h"
#import "JFCAppDelegate.h"
#import "FocusCheckEntry.h"
#import "JFCProgressChart.h"
#import <QuartzCore/QuartzCore.h>

typedef enum {
    JFCPlotTypePie = 0,
    JFCPlotTypeThisWeekLine = 1,
    JFCPlotTypeBar = 2
} JFCPlotType;

@interface JFCChartsViewController ()
{
    NSManagedObjectContext *moCtx;
    NSUInteger focusedCount;
    NSUInteger unfocusedCount;
    CPTXYGraph *graph;
    CPTXYGraph *thisWeekGraph;
    CPTBarPlot *twoWeekBar;
    NSArray *focusEntries; // TODO: Get in batches or limited by date
    JFCPlotType curPlotType;
}
@end

@implementation JFCChartsViewController

@synthesize graphHost;


-(void)initData
{
    NSDate *lastWeek  = [[NSDate date] dateByAddingTimeInterval: -1209600.0];
    NSUInteger total = 0;
    focusedCount = 0;
    unfocusedCount = 0;
    NSFetchRequest *focusEntryRequest = [NSFetchRequest fetchRequestWithEntityName:@"FocusCheckEntry"];
    [focusEntryRequest setPredicate:[NSPredicate predicateWithFormat:@"entryDate > %@", lastWeek]];
    NSSortDescriptor *valDescriptor = [[NSSortDescriptor alloc]
                                       initWithKey:@"focusValue" ascending:YES];
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"entryDate" ascending:YES];
    
    [focusEntryRequest setSortDescriptors:@[dateDescriptor,valDescriptor]];
    NSError * error;
    focusEntries = [moCtx executeFetchRequest:focusEntryRequest error:&error];
    
    //focusEntries = [self generateEntries];
    if (focusEntries == NULL){
        NSLog(@"Error fetching: %@",error);
    } else {
        total = [focusEntries count];
        for (FocusCheckEntry *fc in focusEntries){
            if (fc.focusValue == [NSNumber numberWithInt:1]){
                focusedCount++;
            } else {
                unfocusedCount++;
                
            }
        }
    }
    
}

-(void) drawTwoWeekBar
{
    [graphHost setBackgroundColor:[UIColor grayColor]];
    
    // create the graph
    graph = [[CPTXYGraph alloc] initWithFrame:self.graphHost.superview.bounds];
    graph.plotAreaFrame.masksToBorder = NO;
    self.graphHost.hostedGraph = graph;
    
    // plot space
    CGFloat xMin = 0.0f;
    CGFloat xMax = [focusEntries count];
    CGFloat yMin = 0.0f;
    CGFloat yMax = 2.0f;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xMin) length:CPTDecimalFromFloat(xMax)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin) length:CPTDecimalFromFloat(yMax)];
    
    [graph applyTheme:[CPTTheme themeNamed:kCPTPlainBlackTheme]];
    graph.paddingBottom = 30.0f;
    graph.paddingLeft  = 30.0f;
    graph.paddingTop    = -1.0f;
    graph.paddingRight  = -5.0f;
    
    
    curPlotType = JFCPlotTypeBar;
    
    CPTBarPlot * barChart = [CPTBarPlot tubularBarPlotWithColor:[CPTColor clearColor] horizontalBars:NO];
    barChart.barsAreHorizontal = NO;
   
    
    
    [barChart setIdentifier:@"Percentage of Focused vs Non-Focused"];
    barChart.labelOffset = -0.1;
    
    CPTMutableLineStyle *barLineStyle = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineColor = [CPTColor lightGrayColor];
    barLineStyle.lineWidth = 0.5;
    
    barChart.barWidth = CPTDecimalFromDouble(20.0);
    barChart.barOffset = CPTDecimalFromDouble(0.0);
    barChart.lineStyle = barLineStyle;
    
    
    barChart.delegate = self;
    [barChart setDataSource:self];
    [graph addPlot:barChart];
    
    barChart.identifier = @"Percentage of Focused vs Unfocused";
    
    
    // configure axis
    // 1 - Configure styles
    CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
    axisTitleStyle.color = [CPTColor whiteColor];
    axisTitleStyle.fontName = @"Helvetica-Bold";
    axisTitleStyle.fontSize = 12.0f;
    CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
    axisLineStyle.lineWidth = 2.0f;
    axisLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:1];
    // 2 - Get the graph's axis set
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.graphHost.hostedGraph.axisSet;
    // 3 - Configure the x-axis
    axisSet.xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    axisSet.xAxis.title = @"Dates";
    axisSet.xAxis.titleTextStyle = axisTitleStyle;
    axisSet.xAxis.titleOffset = 10.0f;
    axisSet.xAxis.axisLineStyle = axisLineStyle;
    // 4 - Configure the y-axis
    axisSet.yAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    axisSet.yAxis.title = @"Focused";
    axisSet.yAxis.titleTextStyle = axisTitleStyle;
    axisSet.yAxis.titleOffset = 5.0f;
    axisSet.yAxis.axisLineStyle = axisLineStyle;
  //  graph.legendAnchor = CPTRectAnchorBottom;
    //graph.legendDisplacement = CGPointMake(0.0, 30.0);

}
-(void) drawThisWeekGraph
{
    [graphHost setBackgroundColor:FC_BACKGROUND_COLOR];
    graphHost.clipsToBounds = YES;
    
    thisWeekGraph = [[JFCProgressChart alloc] initWithFrame:graphHost.bounds andEntries:focusEntries];
  
    //pieChart = [[CPTPieChart alloc] initWithFrame:graphHost.bounds];
    curPlotType = JFCPlotTypeBar;
    graphHost.hostedGraph = thisWeekGraph;
    
 /*
     */
    
       
}
-(void) drawPieChart
{

    [graphHost setBackgroundColor:[UIColor redColor]];
    graph = [[CPTXYGraph alloc] initWithFrame:graphHost.bounds];
	//pieChart = [[CPTPieChart alloc] initWithFrame:graphHost.bounds];
    graphHost.hostedGraph = graph;
    curPlotType = JFCPlotTypePie;
    /* CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
     plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-6)
     length:CPTDecimalFromFloat(12)];
     plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-5) 
     length:CPTDecimalFromFloat(30)];*/
    CPTPieChart * pieChart = [[CPTPieChart alloc] initWithFrame:graphHost.bounds];
    [pieChart setIdentifier:@"Percentage of Focused vs Non-Focused"];
    pieChart.labelOffset = -0.1;
    
    pieChart.pieRadius = 80.0;
    pieChart.delegate = self;
    [pieChart setDataSource:self];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [graph applyTheme:theme];
    [graph addPlot:pieChart];
    
    pieChart.identifier = @"Percentage of Focused vs Unfocused";
    
    CPTLegend *theLegend = [CPTLegend legendWithGraph:graph];
    theLegend.numberOfColumns = 2;
    theLegend.fill = [CPTFill fillWithColor:[CPTColor whiteColor]];
    theLegend.borderLineStyle = [CPTLineStyle lineStyle];
    theLegend.cornerRadius = 5.0;
    
    graph.legend = theLegend;
    
    graph.legendAnchor = CPTRectAnchorBottom;
    graph.legendDisplacement = CGPointMake(0.0, 30.0);

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setManagedObjectContext:[(JFCAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext] ];

    self.graphHost = ((CPTGraphHostingView *)[self.view.subviews objectAtIndex:0]);
    [self initData];
  //  [self drawTwoWeekBar];
    [self drawThisWeekGraph];
    //self.view.backgroundColor = [UIColor whiteColor];
    
    // [self drawPieChart];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(void) setManagedObjectContext:(NSManagedObjectContext *)_moCtx
{
    moCtx = _moCtx;
}

// -- CPTP
#pragma TODO use separate data source impementations wrapping the data?
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    if (curPlotType == JFCPlotTypePie){
        return 2;
    } else {
        return [focusEntries count];
    }
}


// Implement one of the following
-(NSNumber *)numberForPlot:(CPTPlot *)plot 
                     field:(NSUInteger)fieldEnum 
               recordIndex:(NSUInteger)index; 
{
    
    
    if (curPlotType == JFCPlotTypePie){
        if ( fieldEnum == CPTPieChartFieldSliceWidth ) {
            return [NSNumber numberWithInt:index == 0 ? focusedCount : unfocusedCount];
        }
        else {
            return [NSNumber numberWithInt:index];
        }
        
    } else {
        if (fieldEnum == CPTScatterPlotFieldX){
            return [NSNumber numberWithInteger:index];
        } else if (fieldEnum == CPTScatterPlotFieldY){
            NSNumber* val = [[focusEntries objectAtIndex:index] focusValue];
            NSLog(@"Val is: %@",val);
            return val;
        }
        NSLog(@"Didn't expect to be hear, fieldEnum is %u",fieldEnum);
        return NULL;
    }
}

-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index
{
	static CPTMutableTextStyle *whiteText = nil;
    
	if ( !whiteText ) {
		whiteText		= [[CPTMutableTextStyle alloc] init];
		whiteText.color = [CPTColor whiteColor];
	}
    if (focusedCount + unfocusedCount > 0){
    NSUInteger focusedPercent = 100 * focusedCount / (focusedCount + unfocusedCount);
    NSUInteger unfocusedPercent = 100 * unfocusedCount / (focusedCount + unfocusedCount);

    
	CPTTextLayer *newLayer = [[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%i", index == 0 ? focusedPercent : unfocusedPercent]
														   style:whiteText];
	return newLayer;
    }
    return nil;
}

-(NSString *)legendTitleForPieChart:(CPTPieChart *)pieChart
                        recordIndex:(NSUInteger)index{
    return index == 0 ? @"Focused" : @"Unfocused";

}

-(IBAction)graphTypeChanged:(id)sender
{
    UISegmentedControl *ctrl = (UISegmentedControl*)sender;
    NSInteger selectedIndex = [ctrl selectedSegmentIndex];
    if (selectedIndex == 0) { // pie
        [self drawPieChart];
    } else  if (selectedIndex == 1) { // this week line
        [self drawThisWeekGraph];
    }
}

// Generate array of FocusCheckEntries for testing
-(NSArray*) generateEntries
{
    NSMutableArray *entries = [NSMutableArray new];
    NSDate *lastTwoWeeks  = [[NSDate date] dateByAddingTimeInterval: (-1209600.0)];
    NSTimeInterval oneDay = 60*60*24*1;
    NSDate *date = lastTwoWeeks;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"FocusCheckEntry" inManagedObjectContext:moCtx];
    
    for (int i = 0; i < (14 * 5); i++){
        FocusCheckEntry *entry = [[FocusCheckEntry alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
        [entry setFocusValue:[NSNumber numberWithUnsignedInt:(arc4random() % 2)]];
        if (i % 5 == 0){
            date = [date dateByAddingTimeInterval:oneDay];
        }
        entry.entryDate = date;
        
        [entries addObject:entry];
    }
    return entries;
}

@end
