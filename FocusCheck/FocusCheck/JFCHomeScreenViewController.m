//
//  JFCHomeScreenControllerViewController.m
//  FocusCheck
//
//  Created by Josh DeWald on 1/21/14.
//
//

#import <QuartzCore/QuartzCore.h>
#import "JFCHomeScreenViewController.h"
#import "JFCAppDelegate.h"
#import "JFCLocalNotificationSupport.h"
#import "FocusCheckSession.h"
#import "JFCMarkFocusController.h"
#import "JFCLocalNotificationSupport.h"
@interface JFCHomeScreenViewController ()

@end

@implementation JFCHomeScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setManagedObjectContext:[(JFCAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext] ];
    
  //  self.quickStartLabel.transform = CGAffineTransformMakeRotation(-(3.141519/2.0));

    //---- round up some stuff
    UIButton *btn = (UIButton*)[self.view viewWithTag:110];


    btn.tintColor = [UIColor whiteColor];
    btn.layer.cornerRadius = 6.0f;
    // needed because the calendar was made with black image
    [btn setImage:[[UIImage imageNamed:@"tab_calendar.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
 
    UIButton* chartsBtn = (UIButton*)[self.view viewWithTag:120];
    chartsBtn.layer.cornerRadius = 6.0f;
    
    UIButton* infoIcn = (UIButton*)[self.view viewWithTag:130];
    infoIcn.layer.cornerRadius = 6.0f;
    
   /* self.toolbar.layer.shadowColor = [UIColor colorWithRed:(158.0/255.0) green:(158.0/255.0) blue:(158.0/255.0) alpha:1.0].CGColor;
    self.toolbar.layer.shadowOffset = CGSizeMake(0,-1);
    self.toolbar.layer.shadowOpacity = 0.8f;
    */
   // self.toolbar.layer.borderColor = [UIColor colorWithRed:(158.0/255.0) green:(158.0/255.0) blue:(158.0/255.0) alpha:1.0].CGColor;
    //self.toolbar.layer.borderWidth = 2.0f;
   
    [self.view viewWithTag:230].layer.cornerRadius = 12.0f;
    [self.view viewWithTag:250].layer.cornerRadius = 12.0f;
        [self.view viewWithTag:100].layer.cornerRadius = 12.0f;
    self.view.backgroundColor = [UIColor colorWithRed:(169.0/255.0) green:(166.0/255.0) blue:(166.0/255.0) alpha:1.0f];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* Make sure it asks the question*/
-(void) jumpToSessionWithFocus:(FocusCheckSession*)jumpToSession
{
    [self performSegueWithIdentifier:@"MarkFocus" sender:jumpToSession];
}

-(void) jumpToSession:(FocusCheckSession *)jumpToSession;
{
    [self performSegueWithIdentifier:@"DisplaySession" sender:jumpToSession];
   
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier hasPrefix:@"quick_"]) {
        [self playButtonSound];
        NSInteger length = 30;
        if ([segue.identifier isEqualToString:@"quick_fifty"]) {
            length = 50;
        }
        FocusCheckSession* session = [FocusCheckSession createSessionWithInterval:length inContext:self.managedObjectContext];
        NSError* err;
        if (![self.managedObjectContext save:&err]) {
            NSLog(@"Unable to save ad hock session: %@", err);
        }
        [JFCLocalNotificationSupport scheduleNotification:session];
       // [JFCLocalNotificationSupport scheduleNotification:session];
        [((JFCMarkFocusController*)segue.destinationViewController) setFocusCheckSession:session];
    } else if ([segue.identifier isEqualToString:@"MarkFocus"]){
        [((JFCMarkFocusController*)segue.destinationViewController) setFocusCheckSession:sender];
        [((JFCMarkFocusController*)segue.destinationViewController) requestFocus];

    } else if ([segue.identifier isEqualToString:@"DisplaySession"]) {
        [((JFCMarkFocusController*)segue.destinationViewController) setFocusCheckSession:sender];
        
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    NSString *expCode = [[NSUserDefaults standardUserDefaults] valueForKey:(NSString*)kKeyUserCode];
    if (! expCode || [expCode length] == 0) {
        [self performSegueWithIdentifier:@"ShowSettings" sender:self];
    }
}

@end
