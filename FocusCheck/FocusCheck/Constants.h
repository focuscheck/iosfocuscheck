//
//  Constants.h
//  FocusCheck
//
//  Created by Joshua DeWald on 2/27/14.
//
//

#ifndef FocusCheck_Constants_h
#define FocusCheck_Constants_h

#define FC_DARKER_RED [UIColor colorWithRed:(138.0/255.0) green:(12.0/255.0) blue:(0.0/255) alpha:1.0]
#define FC_LIGHTER_RED [UIColor colorWithRed:(145.0/255.0) green:(48.0/255.0) blue:(65.0/255) alpha:1.0]
#define FC_BLUE [UIColor colorWithRed:(133.0/255.0) green:(176.0/255.0) blue:(169.0/255.0) alpha:1.0]
#define FC_WHITE [UIColor colorWithRed:(240.0/255.0) green:(244.0/255.0) blue:(243.0/255.0) alpha:1.0]
#define FC_BLACK [UIColor colorWithRed:(5.0/255.0) green:(5.0/255.0) blue:(5.0/255.0) alpha:1.0]

#define FC_GREEN [UIColor colorWithRed:(114.0/255.0) green:(113.0/255.0) blue:(0.0/255.0) alpha:1.0]
#define FC_ORANGE [UIColor colorWithRed:(187.0/255.0) green:(62.0/255.0) blue:(0.0/255.0) alpha:1.0]
#define FC_DARKER_RED [UIColor colorWithRed:(138.0/255.0) green:(12.0/255.0) blue:(0.0/255) alpha:1.0]
#define FC_LIGHT_BLUE [UIColor colorWithRed:(9.0/255.0) green:(68.0/255.0) blue:(116.0/255) alpha:1.0]
#define FC_LIGHT_GRAY [UIColor colorWithRed:(193.0/255.0) green:(197.0/255.0) blue:(200.0/255) alpha:1.0]
#define TRACK_COLOR [UIColor colorWithRed:(64.0/255.0) green:(63.0/255.0) blue:(63.0/255) alpha:1.0]
#define PROGRESS_COLOR [UIColor colorWithRed:(114.0/255.0) green:(113.0/255.0) blue:(0.0/255) alpha:1.0]
#define FC_BACKGROUND_COLOR [UIColor colorWithRed:(205.0/255.0) green:(202.0/255.0) blue:(193.0/255.0) alpha:1.0f]

#define RECUR_SUN  (1UL << 1)
#define RECUR_MONDAY  (1UL << 2)
#define RECUR_TUE  (1UL << 3)
#define RECUR_WED  (1UL << 4)
#define RECUR_THUR  (1UL << 5)
#define RECUR_FRI  (1UL << 6)
#define RECUR_SAT  (1UL << 7)

#define RECUR_ANY  (RECUR_MONDAY | RECUR_TUE | RECUR_WED | RECUR_THUR | RECUR_FRI | RECUR_SAT | RECUR_SUN)


extern const int kReminderInterval;

extern const NSString *kKeyReminderInterval;
extern const NSString *kKeyButtonSound;
extern const NSString *kKeyFocusSound;
extern const NSString *kKeyUserCode;
#endif
