//
//  FocusCheckSession.m
//  FocusCheck
//
//  Created by Joshua DeWald on 9/7/12.
//
//

#import "FocusCheckSession.h"


@implementation FocusCheckSession

@dynamic firstDate;
@dynamic length;
@dynamic nextDate;
@dynamic recurring;
@dynamic sessionType;

+(FocusCheckSession*) createSessionWithInterval:(NSInteger)minutes inContext:(NSManagedObjectContext*)ctx
{
    FocusCheckSession * entry = [NSEntityDescription insertNewObjectForEntityForName:@"FocusCheckSession" inManagedObjectContext:ctx];
    
    entry.firstDate = [[NSDate date] timeIntervalSince1970];
    entry.nextDate = entry.firstDate;
    entry.recurring = 0;
    entry.length = minutes;
    entry.sessionType = @"Quick";
    
    return entry;
}
@end
