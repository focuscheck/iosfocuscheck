//
//  JFCStrategyContentViewController.h
//  FocusCheck
//
//  Created by Josh DeWald on 2/3/14.
//
//

#import <UIKit/UIKit.h>
#import "JFCBaseViewController.h"

@interface JFCStrategyContentViewController : JFCBaseViewController
@property (weak,nonatomic)  IBOutlet    UIView  *textView;
-(IBAction) homeButtonPressed;
@end
