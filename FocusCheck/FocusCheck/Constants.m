#include "Constants.h"

const NSString* kKeyReminderInterval = @"alert_interval";
const NSString *kKeyButtonSound = @"btn_sound_enabled";
const NSString *kKeyFocusSound = @"focus_sound_enabled";
const NSString *kKeyUserCode = @"experiment_code";
const int kReminderInterval = 10; // minutes