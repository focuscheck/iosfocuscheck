//
//  JFCServerSyncSupport.m
//  FocusCheck
//
//  Created by Josh Dewald on 8/28/12.
//
//

#import "JFCServerSyncSupport.h"
#import "JFCAppDelegate.h"
#import "FocusCheckSession.h"

@implementation JFCServerSyncSupport
+(void) sendEntryToServer:(FocusCheckEntry *)entry
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/YYYY HH:mm Z"];

    /*
     String userKey = obj.optString("userKey");
     String sessionType = obj.optString("sessionType");
     int value = obj.optInt("entryValue");
     Long timestamp = obj.optLong("entryTime");
     */
    
    
    NSString *userCode = [[NSUserDefaults standardUserDefaults] valueForKey:(NSString*)kKeyUserCode];
    if (! (userCode != nil && [userCode length] > 0)) {
        userCode = [(JFCAppDelegate*)[[UIApplication sharedApplication] delegate] getUUID];
    }
    
    NSMutableDictionary *fcDict = [[NSMutableDictionary alloc] init];
    [fcDict setValue:entry.focusValue forKey:@"entryValue"];
    [fcDict setValue:[NSNumber numberWithDouble:[entry.entryDate timeIntervalSince1970]] forKey:@"entryTime"];
    [fcDict setValue:[entry.session.objectID.URIRepresentation resourceSpecifier]  forKey:@"sessionType"];
    [fcDict setValue:userCode forKey:@"userKey"];
    //[fcDict setValue:entry.session.objectID.URIRepresentation.description forKey:@"sessionID"];
    
    NSMutableURLRequest *request =
    [[NSMutableURLRequest alloc] initWithURL:
     [NSURL URLWithString:@"http://attentionapptest.appspot.com/focusentry"]];
    
    [request setHTTPMethod:@"POST"];
    
    NSError *err;
    NSData *json = [NSJSONSerialization dataWithJSONObject:fcDict options:0 error:&err];
    if (!json){
        NSLog(@"Unable to generate JSON data, %@", err);
    } else {
     
        [request setHTTPBody:json];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *resp, NSData *respData, NSError *err) {
            if (!err){
                entry.sentToServer = @true;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSError *err2;
                    if (![[entry managedObjectContext] save:&err2]){
                        NSLog(@"Unable to save entry %@", err2);
                    }
                });
               
            }
            
        }];
       
    }

}
@end

