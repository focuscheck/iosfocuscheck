//
//  FocusCheckEntry.h
//  FocusCheck
//
//  Created by Joshua DeWald on 9/7/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FocusCheckSession;

@interface FocusCheckEntry : NSManagedObject

@property (nonatomic, retain) NSDate * entryDate;
@property (nonatomic, retain) NSNumber * focusValue;
@property (nonatomic, retain) NSNumber * sentToServer;
@property (nonatomic, retain) FocusCheckSession *session;

@end
