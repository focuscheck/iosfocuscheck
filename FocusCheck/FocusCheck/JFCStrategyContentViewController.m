//
//  JFCStrategyContentViewController.m
//  FocusCheck
//
//  Created by Josh DeWald on 2/3/14.
//
//

#import "JFCStrategyContentViewController.h"

@interface JFCStrategyContentViewController () <UIWebViewDelegate>

@end

@implementation JFCStrategyContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIWebView *wv = (UIWebView*)[self.view viewWithTag:10];
    wv.delegate = self;
    NSString *filePath = nil;
	if ([[self restorationIdentifier] isEqualToString:@"readingStrategiesController"]) {
        filePath = [[NSBundle mainBundle] pathForResource:@"reading" ofType:@"html"];
    } else if ([[self restorationIdentifier] isEqualToString:@"listeningStrategiesController"]) {
        filePath = [[NSBundle mainBundle] pathForResource:@"listening" ofType:@"html"];
    } else {
        filePath = [[NSBundle mainBundle] pathForResource:@"help" ofType:@"html"];
    }
    [wv loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:filePath]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)homeButtonPressed
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}
@end
