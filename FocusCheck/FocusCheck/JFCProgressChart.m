//
//  JFCProgressChart.m
//  FocusCheck
//
//  Created by Josh Dewald on 7/24/12.
//
//

#import "JFCProgressChart.h"
#import "FocusCheckEntry.h"
#import "JFCStatistic.h"
#import <QuartzCore/QuartzCore.h>

@interface JFCProgressChart ()
@property (nonatomic,retain) NSMutableDictionary *dateStats;
@property (nonatomic,retain) NSMutableArray      *statEntries;
@end

@implementation JFCProgressChart
@synthesize focusEntries, dateStats, statEntries;

-(void)setupStatistics
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd"];
    if (dateStats == nil){
        dateStats = [[NSMutableDictionary alloc] init];
    }
    [dateStats removeAllObjects];
    
    [focusEntries enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        FocusCheckEntry *entry = (FocusCheckEntry*)obj;
        NSString *dateString = [dateFormatter stringFromDate:entry.entryDate];
        JFCStatistic *stat = [dateStats objectForKey:dateString];
        if (stat == nil){
            stat = [[JFCStatistic alloc] initWithLabel:dateString];
            [dateStats setValue:stat forKey:dateString];
        }
        [stat addValue:entry.focusValue];
    }];
    
    if (statEntries == nil){
        statEntries = [[NSMutableArray alloc] initWithCapacity:[dateStats count]];
    }
 
    NSArray* keys = [dateStats keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [[obj1 label] compare:[obj2 label]];
    }];
                     
    [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        JFCStatistic *stat = (JFCStatistic*)[dateStats valueForKey:obj];
        // [statEntries addObject:[stat meanAverage]];
        [statEntries addObject:stat];
    }];
    
  
       
  
    
}

-(id)initWithFrame:(CGRect)frame andEntries:(NSArray *)entries
{
    self = [super initWithFrame:frame];
    if (self){
        self.cornerRadius = 12.0f;
        self.focusEntries = entries;
        [self setupStatistics];
        
        
        CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)self.defaultPlotSpace;
        double xAxisLength = 1.1 * [statEntries count];
        
        plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                        length:CPTDecimalFromFloat(xAxisLength)];
        
        
        
        plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0)
                                                        length:CPTDecimalFromFloat(105)];
        
        
        // -- start bar
      //  curPlotType = JFCPlotTypeBar;
        CPTBarPlot * barChart = [[CPTBarPlot alloc] init];
        barChart.fill = [[CPTFill alloc] initWithColor:[CPTColor colorWithCGColor:FC_ORANGE.CGColor]];
        barChart.barsAreHorizontal = NO;
        self.plotAreaFrame.paddingBottom = 35;
        self.plotAreaFrame.paddingLeft = 35; // higher padding moves stuff to the left, so you can see axis

        
        [self addPlotSpace:plotSpace];
        barChart.labelOffset = 0.0f; // offset of data labels
        
        double spaceAvailable = self.frame.size.width;
        spaceAvailable  -= (self.paddingLeft + self.paddingRight);
    
        double barWidth = 1.0f;//0.25 * spaceAvailable/(2.0 * [statEntries count] );
        barChart.barWidth = CPTDecimalFromDouble(barWidth);
        barChart.barOffset = CPTDecimalFromDouble(1.0); // 2 bar away
        
        
        barChart.delegate = self;
        [barChart setDataSource:self];
        
        
       // [self applyTheme:theme];
        [self addPlot:barChart];
        
        
        CPTMutableTextStyle *legendStyle = [[CPTMutableTextStyle alloc] init];
        legendStyle.fontSize = 10.0f;
        CPTLegend *theLegend = [CPTLegend legendWithGraph:self];
        theLegend.numberOfColumns = 1;
        theLegend.fill = [CPTFill fillWithColor:[CPTColor whiteColor]];
        theLegend.borderLineStyle = [CPTLineStyle lineStyle];
        theLegend.cornerRadius = 5.0;
        theLegend.textStyle = legendStyle;
        [barChart setIdentifier:@"Percentage of Focused vs Non-Focused"];

        
        //self.legend = theLegend;
        
        self.legendAnchor = CPTRectAnchorTop;
        self.legendDisplacement = CGPointMake(0.0, 0.0);
        
        self.plotAreaFrame.backgroundColor = [UIColor clearColor].CGColor;
        self.plotAreaFrame.cornerRadius = 12.0f;
        //self.plotAreaFrame.backgroundColor = FC_BACKGROUND_COLOR.CGColor;
        //FC_BLUE.CGColor;
        
        static CPTMutableTextStyle *whiteText = nil;
        
        if ( !whiteText ) {
            whiteText		= [[CPTMutableTextStyle alloc] init];
            whiteText.color = [CPTColor colorWithCGColor:FC_BLACK.CGColor];
            whiteText.fontSize = 9.0f;
            
        }
        
        static CPTMutableTextStyle *yStyle = nil;
        
        if ( !yStyle ) {
            yStyle		= [[CPTMutableTextStyle alloc] init];
            yStyle.color = [CPTColor colorWithCGColor:FC_BLACK.CGColor];
            yStyle.fontSize = 10.0f;
            
        }

        NSMutableArray *customLabels = [[NSMutableArray alloc] initWithCapacity:[statEntries count]];
        [statEntries enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            JFCStatistic *stat = (JFCStatistic*)obj;
            // [statEntries addObject:[stat meanAverage]];
            CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[stat label] textStyle:whiteText];
            label.rotation = -120.0f;
            label.offset = 0.0f; // vertical offset
            label.tickLocation = CPTDecimalFromInteger(idx+1);
            [customLabels addObject:label];
            
        }];
        
        CPTAxis *xAxis = [self.axisSet.axes objectAtIndex:0];
        xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
        xAxis.title = @"";
        xAxis.axisLabels = [NSSet setWithArray:customLabels];
        xAxis.titleLocation = CPTDecimalFromFloat(0/*[statEntries count] / 2*/);
        xAxis.titleOffset = 15.0f; // vertical offset
        //xAxis.labelOffset = 15.0f;
        
        CPTMutableLineStyle *lineStyle = [CPTMutableLineStyle lineStyle];
        lineStyle.lineColor = [CPTColor blackColor];
        lineStyle.lineWidth = 2.0f;
        

        CPTAxis *yAxis = [self.axisSet.axes objectAtIndex:1];
        yAxis.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        formatter.maximumFractionDigits = 0;
        yAxis.majorIntervalLength = CPTDecimalFromFloat(0.25f);
        yAxis.majorTickLineStyle = lineStyle;
        yAxis.titleLocation = CPTDecimalFromFloat(30.0f);
        yAxis.labelOffset = 0.0;
        yAxis.labelFormatter = formatter;
        
        self.axisSet.delegate = self;

    }
    return self;
}


#pragma TODO use separate data source impementations wrapping the data?
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return [dateStats count];
}

// Implement one of the following
-(NSNumber *)numberForPlot:(CPTPlot *)plot 
                     field:(NSUInteger)fieldEnum 
               recordIndex:(NSUInteger)index; 
{

    if (fieldEnum == CPTScatterPlotFieldX){
        return [NSNumber numberWithInteger:index];
    } else if (fieldEnum == CPTScatterPlotFieldY){
        NSNumber* val = @(100 * [[[statEntries objectAtIndex:index] meanAverage] doubleValue]);
    
        return @([val integerValue]);
    }
    NSLog(@"Didn't expect to be here, fieldEnum is %u",fieldEnum);
    return NULL;
}

-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index
{
    return nil;
    /*
	static CPTMutableTextStyle *whiteText = nil;
    
	if ( !whiteText ) {
		whiteText		= [[CPTMutableTextStyle alloc] init];
		whiteText.color = [CPTColor colorWithCGColor:FC_BLACK.CGColor];
        whiteText.fontSize = 18.0f;
	}
 

    JFCStatistic *stat = [statEntries objectAtIndex:index];
    return [[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%i%%", (int)([stat.meanAverage doubleValue] * 100.0) ] style:whiteText];
     */
            /*  NSUInteger focusedPercent = 100 * focusedCount / (focusedCount + unfocusedCount);
    NSUInteger unfocusedPercent = 100 * unfocusedCount / (focusedCount + unfocusedCount);
    
    
	CPTTextLayer *newLayer = [[CPTTextLayer alloc] initWithText:[NSString stringWithFormat:@"%i", index == 0 ? focusedPercent : unfocusedPercent]
                                                          style:whiteText];
	return newLayer;
   */
}

#pragma mark AxisDelegate


@end
