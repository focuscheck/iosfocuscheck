//
//  JFCSecondViewController.h
//  FocusCheck
//
//  Created by Joshua DeWald on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "JFCBaseViewController.h"
@interface JFCChartsViewController : JFCBaseViewController <CPTPlotDataSource,CPTPieChartDataSource>



-(void) setManagedObjectContext:(NSManagedObjectContext *)moCtx;

-(IBAction)graphTypeChanged:(id)sender;

@property (nonatomic,retain)   IBOutlet    CPTGraphHostingView*    graphHost;


//#pragma mark - CPTPlotDataSource
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot*)plot; 

// Implement one of the following
-(NSNumber *)numberForPlot:(CPTPlot *)plot 
                     field:(NSUInteger)fieldEnum 
               recordIndex:(NSUInteger)index; 

-(NSString *)legendTitleForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index;

@end
