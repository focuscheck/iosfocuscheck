//
//  JFCSettingsViewController.h
//  FocusCheck
//
//  Created by Joshua DeWald on 3/8/14.
//
//

#import "JFCBaseViewController.h"

@interface JFCSettingsViewController : JFCBaseViewController
@property (weak,nonatomic)  IBOutlet    UILabel*    minutes;
@property (weak,nonatomic)  IBOutlet    UISlider*   intervalSlider;
@property (weak,nonatomic)  IBOutlet    UISwitch*   btnSoundSwitch;
@property (weak,nonatomic)  IBOutlet    UISwitch*   focusSoundSwitch;
@property (weak,nonatomic)  IBOutlet    UITextField*    experimentCode;

-(IBAction)minutesChanged:(id)sender;
@end
