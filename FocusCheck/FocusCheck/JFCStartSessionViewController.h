//
//  JFCStartSessionViewController.h
//  FocusCheck
//
//  Created by Josh Dewald on 8/16/12.
//
//

#import <UIKit/UIKit.h>
#import "JFCBaseViewController.h"
#import "External/MultiSelectSegmentedControl/MultiSelectSegmentedControl.h"


@interface JFCStartSessionViewController : JFCBaseViewController <UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSManagedObjectContext *moCtx;
    
}
@property IBOutlet UILabel          *minuteText;
@property IBOutlet UIDatePicker         *datePicker;
@property IBOutlet UIPickerView         *intervalPicker;
//@property IBOutlet UIStepper            *timeChanger;
//@property IBOutlet UISegmentedControl   *typeChanger;
@property IBOutlet  UISlider        *lengthPicker;
@property IBOutlet MultiSelectSegmentedControl   *recurrenceChooser;
@property IBOutlet UISwitch         *recurringSwitch;

-(IBAction)didPressScheduleButton:(id)sender;
-(IBAction)didPressCancelButton:(id)sender;

-(IBAction)minutesChanged:(id)sender;
-(IBAction)recurringChanged:(id)sender;
@end
