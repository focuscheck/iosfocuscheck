//
//  JFCBaseViewController.h
//  FocusCheck
//
//  Created by Joshua DeWald on 2/27/14.
//
//

#import <UIKit/UIKit.h>

@interface JFCBaseViewController : UIViewController
-(void) playButtonSound;
@end
