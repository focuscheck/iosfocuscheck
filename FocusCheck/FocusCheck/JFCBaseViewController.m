//
//  JFCBaseViewController.m
//  FocusCheck
//
//  Created by Joshua DeWald on 2/27/14.
//
//

#import "JFCBaseViewController.h"
#import "JFCAppDelegate.h"

@interface JFCBaseViewController ()

@end

@implementation JFCBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:(169.0/255.0) green:(166.0/255.0) blue:(166.0/255.0) alpha:1.0f];
    [self.view viewWithTag:100].layer.cornerRadius = 12.0f;
    //self.view.backgroundColor = FC_BACKGROUND_COLOR;
	//self.view.backgroundColor = FC_LIGHT_GRAY;
    if (self.navigationItem) {
    
        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_home"] style:UIBarButtonItemStylePlain target:nil action:nil];
        [self.navigationItem setBackBarButtonItem:backButtonItem];
      //  self.navigationItem.titleView.tintColor = [UIColor colorWithRed:(3.0/255.0) green:(37.0/255.0) blue:(60.0/255.0) alpha:1.0f];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) playButtonSound
{
    NSNumber *enabled = [[NSUserDefaults standardUserDefaults] valueForKey:(NSString*)kKeyButtonSound];
    if (enabled == nil || [enabled boolValue]) {
        [((JFCAppDelegate*)[[UIApplication sharedApplication] delegate]) playButtonSound];
    }
}
@end
