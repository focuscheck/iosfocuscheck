 //
//  JFCAppDelegate.m
//  FocusCheck
//
//  Created by Joshua DeWald on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JFCAppDelegate.h"
#import "JFCMarkFocusController.h"
#import "JFCHomeScreenViewController.h"
#import "FocusCheckSession.h"
#import "JFCSessionSupport.h"
#import <AVFoundation/AVFoundation.h>
#import <Instabug/Instabug.h>
@implementation JFCAppDelegate
{
    AVAudioPlayer   *btnPlayer;
}

@synthesize window = _window;
@synthesize 
managedObjectModel,
managedObjectContext,
tabBar;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /*
    UILocalNotification *notif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notif != nil){
        UIViewController *selected = nil;
        for (UIViewController * c in ((UITabBarController
                                      *)self.window.rootViewController).viewControllers){
            if ([c class] == [JFCMarkFocusController class]){
                selected = c;
                
                if ([notif.userInfo valueForKey:@"sessionId"] != nil){
                    NSManagedObjectID *objId = [[self persistenceStoreCoordinator] managedObjectIDForURIRepresentation:[NSURL URLWithString:[notif.userInfo valueForKey:@"sessionId"]]];
                    FocusCheckSession *session = (FocusCheckSession *)[[self managedObjectContext] objectWithID:objId];
                    if (session != nil){
                        [((JFCMarkFocusController*)selected) setFocusCheckSession:session];
                    }
                }
                break;
            }
     
        }
    [self.window.rootViewController performSelectorOnMainThread:@selector(setSelectedViewController:) withObject:selected waitUntilDone:FALSE];
       // self.tabBar.selectedIndex = i;
    }
     */
    [Instabug KickOffWithToken:@"add6a2f2e10d97a45f71c55c5d0c6b67" CaptureSource:InstabugCaptureSourceUIKit FeedbackEvent:InstabugFeedbackEventShake IsTrackingLocation:NO];
    [Instabug setShowScreenshot:YES];
    [Instabug setShowEmail:YES];
    //[[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:(46.0/255.0) green:(46.0/255.0) blue:(46.0/255.0) alpha:1.0f]];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor clearColor]];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init] ];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    // Get rid of any sessions that aren't recurring and have passed
    [JFCSessionSupport cleanupSessions:[self managedObjectContext]];
   // [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Thin" size:16.0f]}];
   // [[UILabel appearance] setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:16.0f]];
 //   [[UIView appearance] setTintColor:FC_DARKER_RED];
   // [[UILabel appearance] setTextColor:FC_BLACK];
    //[[UIButton appearance] setBackgroundColor:[UIColor whiteColor]];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
  /*  UIViewController *selected = nil;
    for (UIViewController * c in self.tabBar.viewControllers){
        if ([c class] == [JFCFirstViewController class]){
            selected = c;
            break;
        }
        
    }
    [self.tabBar performSelectorOnMainThread:@selector(setSelectedViewController:) withObject:selected waitUntilDone:FALSE];
*/
   }
   

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"Received UILocalNotification: %@", notification);
    
    FocusCheckSession *session = nil;
    /*
    for (UIViewController * c in ((UITabBarController
                                   *)self.window.rootViewController).viewControllers){
        if ([c class] == [JFCMarkFocusController class]){
            selected = c;
     */
            if ([notification.userInfo valueForKey:@"sessionId"] != nil){
                NSManagedObjectID *objId = [[self persistenceStoreCoordinator] managedObjectIDForURIRepresentation:[NSURL URLWithString:[notification.userInfo valueForKey:@"sessionId"]]];
                if (objId) {
                    session = (FocusCheckSession *)[[self managedObjectContext] objectWithID:objId];
                } else {
                    NSLog(@"Unable to generate an object id for: %@", [notification.userInfo valueForKey:@"sessionId"]);
                }
               
            }
           /*
            break;
        }
        
    }
    
    [self.window.rootViewController performSelectorOnMainThread:@selector(setSelectedViewController:) withObject:selected waitUntilDone:FALSE];
             */
    
    if (session && [self.window.rootViewController isKindOfClass:[UINavigationController class]]) {

        UINavigationController *nav = (UINavigationController*)self.window.rootViewController;
        
        if (nav.topViewController.presentedViewController) {
            [nav.topViewController dismissViewControllerAnimated:NO completion:nil];
            //[nav.topViewController dismissViewControllerAnimated:YES completion:^{
                if (! [nav.topViewController isKindOfClass:[JFCMarkFocusController class]]) {
                    [nav popToRootViewControllerAnimated:NO];
                    for (UIViewController * c in nav.viewControllers){
                        if ([c class] == [JFCHomeScreenViewController class]){
                            JFCHomeScreenViewController* home = (JFCHomeScreenViewController*)c;
                            if (notification.hasAction) {
                                [home performSelector:@selector(jumpToSessionWithFocus:) withObject:session afterDelay:1];
                            } else {
                                [home performSelector:@selector(jumpToSession:) withObject:session afterDelay:1];

                            }
                            
                        }
                    }
                } else if (notification.hasAction){
                    [((JFCMarkFocusController*)nav.topViewController) requestFocus];
                }
           // }];
        } else {
            if (! [nav.topViewController isKindOfClass:[JFCMarkFocusController class]]) {
                [nav popToRootViewControllerAnimated:NO];
                for (UIViewController * c in nav.viewControllers){
                    if ([c class] == [JFCHomeScreenViewController class]){
                        JFCHomeScreenViewController* home = (JFCHomeScreenViewController*)c;
                        if (notification.hasAction) {
                            [home performSelector:@selector(jumpToSessionWithFocus:) withObject:session afterDelay:1];
                        } else {
                            [home performSelector:@selector(jumpToSession:) withObject:session afterDelay:1];
                            
                        }
                    }
                }
            } else if (notification.hasAction){
                [((JFCMarkFocusController*)nav.topViewController) requestFocus];
            }
        }
        
       
    }
}

- (NSPersistentStoreCoordinator *)persistenceStoreCoordinator {
    if (_psc == nil) {
        NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSURL *storeUrl = [NSURL fileURLWithPath: [documentsDirectory stringByAppendingPathComponent: @"FocusCheck.sqlite"]];
        
        _psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        
        NSError *error = nil;
        if (![_psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl
                                      options:@{NSInferMappingModelAutomaticallyOption:@true,
 NSMigratePersistentStoresAutomaticallyOption:@true} error:&error])
        {
            NSLog(@"Error occured error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    return _psc;
}

- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext == nil) {
        NSPersistentStoreCoordinator *coordinator = [self persistenceStoreCoordinator];
        if (coordinator != nil) {
            managedObjectContext = [[NSManagedObjectContext alloc] init];
            [managedObjectContext setPersistentStoreCoordinator: coordinator];
        }
    }
    return managedObjectContext;
}


- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel == nil) {
        managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    }
    return managedObjectModel;
}

-(void) playButtonSound
{
    if (! btnPlayer) {
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"btn" ofType: @"mp3"];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
        btnPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
        btnPlayer.numberOfLoops = 0; //infinite loop
        btnPlayer.volume = 0.75;
    }
    if (btnPlayer) {
        [btnPlayer play];
    }
}

/**
 * Got this from http://stackoverflow.com/questions/9457731/how-can-we-future-proof-against-ios-6s-upcoming-udid-uuid-removal-if-were-usin
 */
- (NSString *)getUUID
{
    NSString *UUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"uniqueID"];
    if (!UUID)
    {
        CFUUIDRef theUUID = CFUUIDCreate(NULL);
        CFStringRef string = CFUUIDCreateString(NULL, theUUID);
        CFRelease(theUUID);
        UUID = [(__bridge NSString*)string stringByReplacingOccurrencesOfString:@"-"withString:@""];
        [[NSUserDefaults standardUserDefaults] setValue:UUID forKey:@"uniqueID"];
         [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return UUID;
}

@end
