//
//  JFCLocalNotificationSupport.h
//  FocusCheck
//
//  Created by Josh Dewald on 8/20/12.
//
//

#import <Foundation/Foundation.h>
#import "FocusCheckSession.h"

@interface JFCLocalNotificationSupport : NSObject
+(void)scheduleNotification:(FocusCheckSession *)theSession;
+(void)cancelNotification:(FocusCheckSession *)theSession;
+(void)resyncAllReminders:(NSArray *)sessions;
@end
