//
//  JFCSessionsViewController.h
//  FocusCheck
//
//  Created by Josh Dewald on 8/18/12.
//
//

#import <UIKit/UIKit.h>

@interface JFCSessionsViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@end
