//
//  JFCProgressView.m
//  FocusCheck
//
//  Created by Josh DeWald on 1/19/14.
//
//

#import "JFCProgressView.h"

@implementation JFCProgressView

-(void)doInit
{
    self.borderWidth = 30;
    [self setClockWise:YES];
    
    [self setColorTable: @{
                                           NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                           NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):TRACK_COLOR,
                                           NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):PROGRESS_COLOR
                                           }];

}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.contentMode = UIViewContentModeTopLeft;
        [self doInit];
    }
    return self;
}
-(id) init {
    self = [super init];
    if (self){
        [self doInit];
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doInit];
       // [self setProgressImage:[UIImage imageNamed:@"progress"]];
    }
    return self;
}

-(void)setProgress:(float)progress animated:(BOOL)animated
{
    [self setProgress:progress];
   // [super setProgress:progress animated:YES];
   // self.image = [self getCroppedImage:@"progress" forPercentage:progress];
   // [self setProgressImage:];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/*
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
}
*/

/*
-(void)layoutIfNeeded {
  //  [super layoutSubviews];
    if (self.frame.size.height < 10){
        CGRect rect = CGRectMake(self.frame.origin.x,
                             self.frame.origin.y,
                             self.bounds.size.width,
                             self.superview.frame.size.height);
        self.frame = rect;
        [self setFrame:rect];
    }

}
 */

-(UIImage*) getCroppedImage:(NSString*) imageName forPercentage:(CGFloat)percentage
{
    if (percentage < 0.00001) {
        return nil;
    }
    UIImage *image = [UIImage imageNamed:imageName];
    
    // Get size of current image
    CGSize size = [image size];
    
    
    // Create rectangle that represents a cropped image
    // from the middle of the existing image
    CGRect rect = CGRectMake(0, 0,
                             size.width * percentage, size.height);
    
    // Create bitmap image from original image data,
    // using rectangle to specify desired crop area
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return img;
    
}


@end
