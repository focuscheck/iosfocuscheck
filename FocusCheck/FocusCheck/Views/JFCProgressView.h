//
//  JFCProgressView.h
//  FocusCheck
//
//  Created by Josh DeWald on 1/19/14.
//
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"

@interface JFCProgressView : KAProgressLabel

-(void)setProgress:(CGFloat)progress animated:(BOOL)animated;
@end
