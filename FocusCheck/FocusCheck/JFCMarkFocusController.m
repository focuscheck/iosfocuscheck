//
//  JFCFirstViewController.m
//  FocusCheck
//
//  Created by Joshua DeWald on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JFCMarkFocusController.h"
#import "FocusCheckEntry.h"
#import "FocusCheckSession.h"
#import "JFCAppDelegate.h"
#import "JFCProgressView.h"
#import "JFCLocalNotificationSupport.h"
#import "JFCServerSyncSupport.h"
#import "JFCSessionSupport.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#define YES_TAG 10
#define NO_TAG 20

@interface JFCMarkFocusController () 
{
    FocusCheckSession       *mSession;
    NSTimeInterval          sessionInterval;
    NSTimeInterval          sessionLength;
    NSTimeInterval          startTime;
    NSTimeInterval          intervalStartTime;
    NSDate                  *startDate;
    BOOL                    shouldRequestFocus;
    AVAudioPlayer           *onTaskPlayer;
    AVAudioPlayer           *donePlayer;
    NSTimer                 *intervalTimer;
    BOOL                    focusSoundEnabled;
}
@end


@implementation JFCMarkFocusController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // preferably this would get injected, but Storyboard seems to take away some features
    [self setManagedObjectContext:[(JFCAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext] ];
    
   

    startDate = [NSDate date];
    NSNumber *userInterval = [[NSUserDefaults standardUserDefaults] objectForKey:@"alert_interval"];
    sessionInterval = 60  * (userInterval != nil ? [userInterval intValue] : kReminderInterval);
    //sessionInterval = 10; // seconds
    //sessionLength = sessionInterval * 4;
    startTime = [startDate timeIntervalSince1970];
    intervalStartTime = startTime;
    self.questionWrapper.hidden = YES;
    [self.intervalProgress setProgress:0.0 animated:YES];
    self.intervalProgress.superview.hidden = YES;

    self.intervalProgress.superview.clipsToBounds = YES;
    self.intervalProgress.superview.layer.cornerRadius = 10.0f;
    self.sessionProgress.superview.clipsToBounds = YES;
    self.sessionProgress.superview.layer.cornerRadius = 10.0f;
    [self.sessionProgress setText:@"0"];
    

    
    for (UIView * v in self.questionWrapper.subviews) {
        v.layer.cornerRadius = 6.0f;
    }
    
    self.timeToFocusWrapper.layer.cornerRadius = self.timeToFocusWrapper.frame.size.width / 2.0f;

   // self.viewWrapper.layer.borderWidth = 2.0f;
    //self.viewWrapper.layer.borderColor = [UIColor grayColor].CGColor;
    self.viewWrapper.layer.cornerRadius = 3.0f;
    
    
    self.view.backgroundColor = [UIColor colorWithRed:(169.0/255.0) green:(166.0/255.0) blue:(166.0/255.0) alpha:1.0f];
    /*
  //  CALayer *layer = [[CALayer alloc] init];
   // [layer setFrame:CGRectMake(0,0,self.view.frame.size.width,50.0)];
    CAGradientLayer *topGradient = [CAGradientLayer layer];
    topGradient.colors = @[
                           (id)[UIColor colorWithRed:(205.0/255.0) green:(202.0/255.0) blue:(139.0/255.0) alpha:1.0f].CGColor,
                           (id)[UIColor redColor].CGColor
           ];
    //topGradient.locations = @[@0,@(0.5)];
    topGradient.bounds = self.view.bounds;
    topGradient.startPoint = CGPointMake(0,0);
    topGradient.endPoint = CGPointMake(0.0,1.0);
    [self.view.layer insertSublayer:topGradient atIndex:0];
     */
    
    if (self.navigationItem) {
        self.navigationItem.hidesBackButton = YES;
   //     self.navigationController.navigationBar.titleTextAttributes = @
    }
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"on_task" ofType: @"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:soundFilePath ];
    onTaskPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
    onTaskPlayer.numberOfLoops = 0; //infinite loop

    NSString *doneFilePath = [[NSBundle mainBundle] pathForResource:@"done" ofType: @"mp3"];
    NSURL *doneFileUrl = [[NSURL alloc] initFileURLWithPath:doneFilePath ];
    donePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:doneFileUrl error:nil];
    donePlayer.numberOfLoops = 0; //infinite loop

    
}

-(void)showDone
{
    if (focusSoundEnabled){
        [donePlayer play];
    }
    [UIView transitionFromView:self.sessionProgress toView:self.doneView duration:2 options:UIViewAnimationOptionTransitionFlipFromRight|UIViewAnimationOptionShowHideTransitionViews completion:^(BOOL finished) {
        [self performSelector:@selector(closeUs) withObject:nil afterDelay:6];
    }];
}

-(void)showQuestion
{
  //  self.questionWrapper.hidden = NO;

    
    

    // self.questionWrapper.alpha = 1.0f;
     // rotate mode, but has weird issue second time around
    [UIView transitionFromView:self.sessionProgress toView:self.timeToFocusWrapper duration:2 options:UIViewAnimationOptionTransitionFlipFromRight|UIViewAnimationOptionShowHideTransitionViews completion:^(BOOL finished) {

        self.questionWrapper.alpha = 0.0f;
        self.questionWrapper.hidden = NO;
        [UIView animateWithDuration:1 animations:^{
            self.questionWrapper.alpha = 1.0f;
           // self.timeToFocusWrapper.alpha = 0.0f;
            
        } completion:^(BOOL finished) {
          //  self.timeToFocusWrapper.hidden = YES;
        }];
        if (focusSoundEnabled){
            [onTaskPlayer play];
        }
        //
    }];
    
}

-(void)hideQuestion
{
   /*
    // this mode uses transition
    self.questionWrapper.hidden = NO;
    self.timeToFocusWrapper.hidden = NO;
    self.questionWrapper.alpha = 1.0f; // 0
    */
    [UIView animateWithDuration:0.5 animations:^{
        self.questionWrapper.alpha = 0.0f;
        //  self.timeToFocusWrapper.alpha = 1.0f;
    } completion:^(BOOL finished) {
        self.questionWrapper.hidden = YES;
        [UIView transitionFromView:self.timeToFocusWrapper toView:self.sessionProgress duration:2 options:UIViewAnimationOptionTransitionFlipFromLeft|UIViewAnimationOptionShowHideTransitionViews completion:^(BOOL finished) {
            self.questionWrapper.alpha = 1.0f;
            //  self.timeToFocusWrapper.alpha = 0.0f;
            //self.timeToFocusWrapper.hidden = NO;
           
        }];
    }];
    
    
    // use simple fade
   
    
   
}

-(void)requestFocus
{
    shouldRequestFocus = YES;
}

-(void) intervalTimerFired:(NSTimer*) timer {
    NSTimeInterval cur = [[NSDate date] timeIntervalSince1970];

    NSTimeInterval intervalDiff = (cur - intervalStartTime); // how long since this particular interval started
    double percentage = (double)intervalDiff / (double)sessionInterval;
  
   
    [self.intervalProgress setProgress:percentage animated:YES];
    if (percentage >= .999) {
        //[timer invalidate];
        [self showQuestion];
        intervalStartTime = [[NSDate date] timeIntervalSince1970];
    }
    

    [self updateProgress];
}

                                                                              
-(void) viewDidAppear:(BOOL)animated
{

    [super viewDidAppear:animated];

    // while this screen is showing, we want it to stay on
    [UIApplication sharedApplication].idleTimerDisabled = YES;

    NSNumber *enabled = [[NSUserDefaults standardUserDefaults] valueForKey:(NSString*)kKeyFocusSound];
    focusSoundEnabled = enabled == nil || [enabled boolValue];
      NSDate *now = [NSDate date];
    if (mSession){
        
        sessionLength = mSession.length * 60; // session lengths are in minutes
        
     
       // sessionLength = 400;
        // Take the session's date, and assume that
        
        startDate = [JFCSessionSupport findNextDate:mSession];
        startTime = [startDate timeIntervalSince1970];
        
        NSTimeInterval cur = [now timeIntervalSince1970];
        NSTimeInterval diff = (cur - startTime);
        // handle case where they click on an alert after too long of time
        if (diff > (sessionLength + 10) ) { // wiggle room
            [self closeUs];

        }
        //startTime = mSession.nextDate;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterShortStyle];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        //intervalStartTime = startTime;
        self.sessionTypeLabel.text = [NSString stringWithFormat:@"%@ for %i minutes from %@", mSession.sessionType, mSession.length, [dateFormatter stringFromDate:startDate]];
        [self updateProgress];

    } else {
        self.sessionTypeLabel.text = @"";
    }
    intervalStartTime = [now timeIntervalSince1970];
    intervalTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(intervalTimerFired:) userInfo:nil repeats:YES];
    if (shouldRequestFocus){
        [self showQuestion];
        shouldRequestFocus = NO;
    }
}

-(void) updateProgress
{
    NSTimeInterval cur = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval diff = (cur - startTime);
    double sessionPercentage = (double)diff / (double)sessionLength;
    int remaining = 1 + (int)((sessionLength - diff)/60);
    if (remaining < 0) {
        remaining = 0; // don't let it go negative. The sessionPercentage should catch it next time around
    }
    [self.sessionProgress setText:[NSString stringWithFormat:@"%i", remaining]];
    [self.sessionProgress setProgress:sessionPercentage animated:YES];
    if (sessionPercentage >= 1.0){
        [intervalTimer invalidate];
        [self showDone];
       
    }
   



}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    // be good citizens
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    [onTaskPlayer stop];
    onTaskPlayer = nil;
    
    if (intervalTimer) {
        [intervalTimer invalidate];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(void) endSessionButtonPressed:(id)sender
{
    [self playButtonSound];
    [onTaskPlayer stop];
    onTaskPlayer = nil;
    [donePlayer stop];
    donePlayer = nil;
    if (intervalTimer) {
        [intervalTimer invalidate];
    }

    // When a "stop" is requested, it is essentially signalling that they are done with
    // this "instance" of the session. If it's a recurring session, then there will be
    // other ones, so we should reschedule our local notificcations to reflect that
    // if it's not recurring, then we don't need to reschedule
    if (mSession){
        [JFCLocalNotificationSupport cancelNotification:mSession];
        NSDate* endDate = [startDate dateByAddingTimeInterval:(mSession.length * 60)];
       
        NSDate *next = [JFCSessionSupport findNextDate:mSession greaterThan:endDate];
        if (next) {
            mSession.nextDate = [next timeIntervalSince1970];
            NSError *err;
            if (![moCtx save:&err]) {
                NSLog(@"Unable to move to next date after cancel: %@", err);
            } else {
                [JFCLocalNotificationSupport scheduleNotification:mSession];
            }
        }
        
    }
    // go back home
    [self closeUs];
}

-(void) closeUs
{
    if ([self presentingViewController]){
        [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    } else if (self.navigationItem) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)focusButtonPressed:(id)sender
{
    [self playButtonSound];
    UIButton *btn = (UIButton*)sender;
     FocusCheckEntry * entry = [NSEntityDescription insertNewObjectForEntityForName:@"FocusCheckEntry" inManagedObjectContext:moCtx];
    [entry setEntryDate:[NSDate date]];
    [entry setSentToServer:[NSNumber numberWithBool:false]];
    if (mSession != nil){
        entry.session = mSession;
        
        mSession = nil;
    }
if (btn.tag == YES_TAG){
    NSLog(@"YES pressed");
    [entry setFocusValue:[NSNumber numberWithInt:1]];
    
} else if (btn.tag == NO_TAG){
    NSLog(@"NO pressed");
    [entry setFocusValue:[NSNumber numberWithInt:0]];
}
    NSError * error;
    if (! [moCtx save:&error]) {
        NSLog(@"Unable to save %@",error);
    } else {
        [JFCServerSyncSupport sendEntryToServer:entry];
    }
    [self hideQuestion];
}

-(void) setManagedObjectContext:(NSManagedObjectContext *)_moCtx
{
    moCtx = _moCtx;
}

-(void) setFocusCheckSession:(FocusCheckSession*)session
{
    NSLog(@"Received session %@", session);
    mSession = session;
}
@end
