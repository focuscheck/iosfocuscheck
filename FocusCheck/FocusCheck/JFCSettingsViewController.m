//
//  JFCSettingsViewController.m
//  FocusCheck
//
//  Created by Joshua DeWald on 3/8/14.
//
//

#import "JFCSettingsViewController.h"

@interface JFCSettingsViewController ()

@end

@implementation JFCSettingsViewController {

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *alertValue = [defaults valueForKey:(NSString*)kKeyReminderInterval] ;
    if (! alertValue) {
        alertValue = @(kReminderInterval);
    }
    
    NSNumber *btnValue = [defaults valueForKey:(NSString*)kKeyButtonSound];
    if (! btnValue) {
        btnValue = @(TRUE);
    }
    
    NSNumber *focusValue = [defaults valueForKey:(NSString*)kKeyFocusSound];
    if (! focusValue) {
        focusValue = @(TRUE);
    }
    [self.intervalSlider setValue:[alertValue floatValue]];
    [self.btnSoundSwitch setOn:[btnValue boolValue]];
    [self.focusSoundSwitch setOn:[focusValue boolValue]];
    self.experimentCode.text = [defaults valueForKey:(NSString*)kKeyUserCode];
    self.minutes.text = [NSString stringWithFormat:@"%i min", [alertValue integerValue]];

}

-(void) viewWillDisappear:(BOOL)animated
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *minValue = @((int)(self.intervalSlider.value));
    [defaults setValue:minValue forKey:(NSString*)kKeyReminderInterval];
    [defaults setValue:@(self.btnSoundSwitch.on) forKey:(NSString*)kKeyButtonSound];
    [defaults setValue:@(self.focusSoundSwitch.on) forKey:(NSString*)kKeyFocusSound];
    [defaults setValue:self.experimentCode.text forKey:(NSString*)kKeyUserCode];
    [defaults synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)minutesChanged:(id)sender
{
    NSInteger minValue = (int)(((UISlider*)sender).value);
    self.minutes.text = [NSString stringWithFormat:@"%i min", minValue];
}
@end
