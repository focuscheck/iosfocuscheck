//
//  JFCStatistic.h
//  FocusCheck
//
//  Created by Josh Dewald on 7/24/12.
//
//

#import <Foundation/Foundation.h>

@interface JFCStatistic : NSObject
@property (nonatomic,retain) NSString *label;

-(id)initWithLabel:(NSString *)label;

-(void)addValue:(NSNumber *)value;

-(NSNumber*)meanAverage;
@end
