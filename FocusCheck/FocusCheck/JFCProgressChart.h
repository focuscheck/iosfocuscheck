//
//  JFCProgressChart.h
//  FocusCheck
//
//  Created by Josh Dewald on 7/24/12.
//
//

#import <Foundation/Foundation.h>
#import "CorePlot-CocoaTouch.h"

@interface JFCProgressChart : CPTXYGraph <CPTPlotDataSource,CPTAxisDelegate>
@property (nonatomic) NSArray *focusEntries;

-(id)initWithFrame:(CGRect)frame andEntries:(NSArray*)entries;

//#pragma mark - CPTPlotDataSource
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot*)plot; 

// Implement one of the following
-(NSNumber *)numberForPlot:(CPTPlot *)plot 
                     field:(NSUInteger)fieldEnum 
               recordIndex:(NSUInteger)index;
@end
