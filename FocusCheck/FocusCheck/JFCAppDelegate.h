//
//  JFCAppDelegate.h
//  FocusCheck
//
//  Created by Joshua DeWald on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
@interface JFCAppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>
{
    NSPersistentStoreCoordinator *_psc;
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) IBOutlet UITabBarController *tabBar;

@property (nonatomic, retain) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

- (NSString *)getUUID;
- (void) playButtonSound;
@end
