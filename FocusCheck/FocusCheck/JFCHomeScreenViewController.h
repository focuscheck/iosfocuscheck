//
//  JFCHomeScreenControllerViewController.h
//  FocusCheck
//
//  Created by Josh DeWald on 1/21/14.
//
//

#import <UIKit/UIKit.h>
#import "JFCBaseViewController.h"
#import "FocusCheckSession.h"

@interface JFCHomeScreenViewController : JFCBaseViewController
@property NSManagedObjectContext        *managedObjectContext;
@property IBOutlet   UILabel            *quickStartLabel;
@property IBOutlet   UIView             *toolbar;
-(void) jumpToSession:(FocusCheckSession*)session;
-(void) jumpToSessionWithFocus:(FocusCheckSession*)session;
@end
