//
//  JFCLocalNotificationSupport.m
//  FocusCheck
//
//  Created by Josh Dewald on 8/20/12.
//
//

#import "JFCLocalNotificationSupport.h"


@implementation JFCLocalNotificationSupport

/*
 * Create all necessary UILocalNotifications corresponding to session entry, accounting for reminder settings
 */
+(void)scheduleNotification:(FocusCheckSession *)theSession
{
    NSNumber *userInterval = [[NSUserDefaults standardUserDefaults] objectForKey:(NSString*)kKeyReminderInterval];
   
    int interval = userInterval != nil ? [userInterval intValue] : kReminderInterval;
    // schedule for every 5 minutes, which isn't directly supported so we need to schedule multiple ones
    
    NSTimeInterval date = theSession.nextDate;
    
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDate *fromDate = [NSDate dateWithTimeIntervalSince1970:date];
    NSDateComponents *components = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit | NSWeekOfMonthCalendarUnit)  fromDate:fromDate];


    NSMutableArray *dates = [[NSMutableArray alloc] init];
    if (theSession.recurring & RECUR_ANY) {
        if (theSession.recurring & RECUR_SUN) {
            [components setWeekday:1];
            [dates addObject:[gregorian dateFromComponents:components]];
        }
        if (theSession.recurring & RECUR_MONDAY) {
            [components setWeekday:2];
            [dates addObject:[gregorian dateFromComponents:components]];
        }
        if (theSession.recurring & RECUR_TUE) {
            [components setWeekday:3];
            [dates addObject:[gregorian dateFromComponents:components]];
        }
        if (theSession.recurring & RECUR_WED) {
            [components setWeekday:4];
            [dates addObject:[gregorian dateFromComponents:components]];
        }
        if (theSession.recurring & RECUR_THUR) {
            [components setWeekday:5];
            [dates addObject:[gregorian dateFromComponents:components]];
        }
        if (theSession.recurring & RECUR_FRI) {
            [components setWeekday:6];
            [dates addObject:[gregorian dateFromComponents:components]];
        }
        if (theSession.recurring & RECUR_SAT) {
            [components setWeekday:7];
            [dates addObject:[gregorian dateFromComponents:components]];
        }
    } else {
        [dates addObject:[NSDate dateWithTimeIntervalSince1970:date]];
    }
    
    // Schedule N instances in advance
    
    [dates enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDate *thisDate = (NSDate*)obj;
        NSLog(@"Scheduling alerts for %@", thisDate);
        NSTimeInterval date = [thisDate timeIntervalSince1970];
        NSTimeInterval endDate = date + (theSession.length * 60); // length is in minutes

        while (endDate > date){
            UILocalNotification *notif = [[UILocalNotification alloc] init];
            
            notif.fireDate = [NSDate dateWithTimeIntervalSince1970:date];
            if (date == theSession.nextDate){
                notif.alertBody = [NSString stringWithFormat:@"Your %@ is about to start!", theSession.sessionType];
                notif.hasAction = NO; // just tell them
            } else {
                notif.alertAction = @"Mark Focus";
                notif.alertBody = @"Are you focused?";
            }
            
            //notif.repeatInterval = theSession.recurri++ng;
            
             if (theSession.recurring > 0){
                 notif.repeatInterval = NSWeekCalendarUnit;
             } else {
                 notif.repeatInterval = 0; // don't repeat
             }
            
            notif.soundName = @"on_task.mp3";
            notif.timeZone = [NSTimeZone systemTimeZone];
            notif.userInfo = @{
                               @"sessionType":theSession.sessionType,
                               @"sessionId":[[[theSession objectID] URIRepresentation] description]};
            [[UIApplication sharedApplication] scheduleLocalNotification:notif];
            
            
            NSLog(@"Scheduled for %@",notif.fireDate);
            
            date += (interval * 60);
        }
        
        
   }
     ];
}

+(void)cancelNotification:(FocusCheckSession *)theSession
{
    NSString *sessionId = [[[theSession objectID] URIRepresentation] description];
    NSArray *notfications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    [notfications enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UILocalNotification *n = (UILocalNotification *)obj;
        if (n.userInfo && [[n.userInfo objectForKey:@"sessionId"] isEqualToString:sessionId]){
            NSLog(@"Cancelling %@", n);
            [[UIApplication sharedApplication] cancelLocalNotification:n];
        }
    }];
}

+(void)resyncAllReminders:(NSArray *)sessions
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    for (FocusCheckSession *s in sessions){
        [JFCLocalNotificationSupport scheduleNotification:s];
    }
}


@end
