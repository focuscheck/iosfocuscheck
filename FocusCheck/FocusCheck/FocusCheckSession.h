//
//  FocusCheckSession.h
//  FocusCheck
//
//  Created by Joshua DeWald on 9/7/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FocusCheckSession : NSManagedObject

@property (nonatomic) NSTimeInterval firstDate;
@property (nonatomic) int16_t length;
@property (nonatomic) NSTimeInterval nextDate;
@property (nonatomic) int16_t recurring;
@property (nonatomic, retain) NSString * sessionType;


+(FocusCheckSession*) createSessionWithInterval:(NSInteger)minutes inContext:(NSManagedObjectContext*)ctx;
@end
