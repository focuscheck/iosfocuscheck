//
//  JFCServerSyncSupport.h
//  FocusCheck
//
//  Created by Josh Dewald on 8/28/12.
//
//

#import <Foundation/Foundation.h>
#import "FocusCheckEntry.h"

@interface JFCServerSyncSupport : NSObject
+(void) sendEntryToServer:(FocusCheckEntry *)entry;
@end
