//
//  JFCSessionsViewController.m
//  FocusCheck
//
//  Created by Josh Dewald on 8/18/12.
//
//

#import "JFCSessionsViewController.h"
#import "JFCAppDelegate.h"
#import "FocusCheckSession.h"
#import "JFCLocalNotificationSupport.h"
#import "JFCSessionSupport.h"

@interface JFCSessionsViewController () 
{
    NSManagedObjectContext *moCtx;
    NSFetchedResultsController *sessionsResultsController;
}
@end

@implementation JFCSessionsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)addItem:(id)sender
{
    [self performSegueWithIdentifier:@"AddSession" sender:self];

}

-(void)showSettings:(id)sender
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.title = @"Scheduled Sessions";
    [self setManagedObjectContext:[(JFCAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext] ];
    

    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FocusCheckSession"];
    request.predicate = [NSPredicate predicateWithFormat:@"sessionType <> 'Quick'"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nextDate" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    sessionsResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:moCtx
                                    sectionNameKeyPath:nil cacheName:nil];
    sessionsResultsController.delegate = self;
    NSError *error;
    if (![sessionsResultsController performFetch:&error]){
        
        NSLog(@"Failure during fetch request: %@",error);
    }
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addItem:)];

 /*      */
    /*NSString *settingsTitle = @"\u2699";
    UIFont *f1 = [UIFont fontWithName:@"Helvetica" size:24.0];
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:f1, UITextAttributeFont, nil];
    
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithTitle:settingsTitle style:UIBarButtonItemStylePlain target:self action:@selector(showSettings:)];
    
    [settingsButton setTitleTextAttributes:dict forState:UIControlStateNormal];
     
    self.navigationItem.rightBarButtonItems = @[settingsButton,addButton];
     */
    //self.navigationItem.rightBarButtonItem = addButton;
    self.navigationItem.rightBarButtonItems = @[addButton, self.editButtonItem];

    
}

- (void) viewDidAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) setManagedObjectContext:(NSManagedObjectContext *)_moCtx
{
    moCtx = _moCtx;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[sessionsResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [[sessionsResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SessionCellSchedule"];
    FocusCheckSession *theSession = [sessionsResultsController objectAtIndexPath:indexPath];
    
    NSDate *sessionDate = [NSDate dateWithTimeIntervalSince1970:theSession.firstDate];
    
    NSDate *endTime = [sessionDate dateByAddingTimeInterval:theSession.length * 60];
    
    NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
    [dayFormatter  setDateFormat:@"EEE"];
    NSDateFormatter *dayAndDateFormatter = [[NSDateFormatter alloc] init];
    [dayAndDateFormatter setDateFormat:@"EEE M/dd"];
    
    NSDateFormatter *firstTimeFormatter = [[NSDateFormatter alloc] init];
    [firstTimeFormatter setDateFormat:@"hh:mm"];
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    
    NSDateFormatter *fullDayFormatter = [[NSDateFormatter alloc] init];
    [fullDayFormatter setDateFormat:@"EEEE's'"];
    
    NSString *dayString = [dayFormatter stringFromDate:sessionDate];
    if (theSession.recurring){
        
        switch (theSession.recurring){
            case NSWeekCalendarUnit:
                dayString = [fullDayFormatter stringFromDate:sessionDate];//[dayString stringByAppendingString:@"s"];
                break;
            case NSDayCalendarUnit:
                dayString = @"Daily";
            default:
                if (theSession.recurring & RECUR_ANY) {
                    dayString = @"";
                    
                    if ((theSession.recurring & RECUR_ANY) == RECUR_ANY) {
                        dayString = @"Daily";
                    } else {
                    if (theSession.recurring & RECUR_SUN) {
                        dayString = [dayString stringByAppendingString:@"Su "];
                    }
                    if (theSession.recurring & RECUR_MONDAY) {
                        dayString = [dayString stringByAppendingString:@"M "];
                    }
                    if (theSession.recurring & RECUR_TUE) {
                        dayString = [dayString stringByAppendingString:@"Tu "];
                    }
                    if (theSession.recurring & RECUR_WED) {
                        dayString = [dayString stringByAppendingString:@"W "];
                    }
                    if (theSession.recurring & RECUR_THUR) {
                        dayString = [dayString stringByAppendingString:@"Th "];
                    }
                    if (theSession.recurring & RECUR_FRI) {
                        dayString = [dayString stringByAppendingString:@"F "];
                    }
                    if (theSession.recurring & RECUR_SAT) {
                        dayString = [dayString stringByAppendingString:@"Sa "];
                    }
                    }
                }
                break;
        }
    } else {
        dayString = [dayAndDateFormatter stringFromDate:sessionDate];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"M/dd hh:mm a"];

    NSString *intervalString = [[firstTimeFormatter stringFromDate:sessionDate] stringByAppendingFormat:@" - %@", [timeFormatter stringFromDate:endTime]];
 

    //NSString *dateString = [dateFormatter stringFromDate:sessionDate];

 
    
    //[(UILabel*)[cell viewWithTag:10] setText:dateString];
    //[(UILabel*)[cell viewWithTag:20] setText:intervalString];
    [(UILabel*)[cell viewWithTag:10] setText:intervalString];
    [(UILabel*)[cell viewWithTag:30] setText:dayString];
    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section { 
    id <NSFetchedResultsSectionInfo> sectionInfo = [[sessionsResultsController sections] objectAtIndex:section];
    return [sectionInfo name];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [sessionsResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [sessionsResultsController sectionForSectionIndexTitle:title atIndex:index];
}

-(void) controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [[self tableView] reloadData];
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        FocusCheckSession *theSession = [sessionsResultsController objectAtIndexPath:indexPath];
        [JFCLocalNotificationSupport cancelNotification:theSession];
        [moCtx deleteObject:theSession];
        NSError *error;
        if (![moCtx save:&error]){
            NSLog(@"Unable to save: %@", error);
        }
      //  [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
       
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}



@end
