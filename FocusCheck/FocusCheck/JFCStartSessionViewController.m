//
//  JFCStartSessionViewController.m
//  FocusCheck
//
//  Created by Josh Dewald on 8/16/12.
//
//

#import "JFCStartSessionViewController.h"
#import "JFCAppDelegate.h"
#import "FocusCheckSession.h"
#import "JFCLocalNotificationSupport.h"

const NSInteger kComponentType = -1;
const NSInteger kComponentLength = 1;
const NSInteger kComponentDay = 0;
const NSInteger kComponentHours = 0;
const NSInteger kComponentMinutes = 1;

@interface JFCStartSessionViewController ()
@property (nonatomic) NSArray *dayOptions;
@property (nonatomic) NSArray *hourOptions;
@property (nonatomic) NSArray *minuteOptions;
@property (nonatomic) NSArray *periodOptions;
@property (nonatomic) NSArray *lengthOptions;
@property (nonatomic) NSArray *typeOptions;
-(void)setupData;
@end

@implementation JFCStartSessionViewController

-(void)setupData
{
    self.dayOptions = @[@"Now",@"Sun",@"Mon",@"Tue",@"Wed",@"Thu",@"Fri",@"Sat"];
    self.hourOptions = @[@"0",@"1",@"2",@"3",@"4"];
    self.minuteOptions = @[@"0",@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"50",@"55"];
    //self.periodOptions = @[@"AM",@"PM"];
    self.lengthOptions = @[@"15",@"30",@"45",@"60",@"75",@"90",@"120"];
    self.typeOptions = @[@"Lecture",@"Reading"];
   // [self.datePicker setDate:[NSDate date]];

}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self){
        [self setupData];
    }
    return self;
}

-(id)init {
    self = [super init];
    if (self){
        [self setupData];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupData];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if ([self presentingViewController] && self.navigationItem != nil){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:(UIBarButtonSystemItemCancel) target:self action:@selector(cancelAdd)];
    }
    
    self.recurrenceChooser.hidden = ! self.recurringSwitch.on;
    [self.datePicker setMinimumDate:[NSDate date]];
    
    int minutes = [((UISlider*)self.lengthPicker) value];
    self.minuteText.text = [NSString stringWithFormat:@"%i min",minutes];

    [self setManagedObjectContext:[(JFCAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext] ];

}

-(void) cancelAdd {
    if ([self presentingViewController]){
        [[self presentingViewController] dismissViewControllerAnimated:YES completion:^{
            //code
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) minutesChanged:(id)sender
{
    int minutes = [((UISlider*)sender) value];
    self.minuteText.text = [NSString stringWithFormat:@"%i min",minutes];
    
}

-(void) didPressCancelButton:(id)sender
{
    if (self.presentingViewController) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
}

-(void) recurringChanged:(id)sender
{
    self.recurrenceChooser.hidden = ! ((UISwitch*)sender).on;
    
}

-(void) didPressScheduleButton:(id)sender
{
    int minutes = [((UISlider*)self.lengthPicker) value];

    //int minutes = [(self.minuteOptions[[self.lengthPicker selectedRowInComponent:kComponentMinutes]]) intValue];
    //minutes += ([(self.hourOptions[[self.lengthPicker selectedRowInComponent:kComponentHours]]) intValue] * 60);
    
    
    NSDate *scheduleDate = self.datePicker.date;
    
 //   NSString *type = [self.typeOptions objectAtIndex:self.typeChanger.selectedSegmentIndex];
  //  NSString *day = [self.dayOptions objectAtIndex:[self.intervalPicker selectedRowInComponent:kComponentDay]];
    
      
    NSCalendar *cal = [NSCalendar currentCalendar];
    cal.timeZone = [NSTimeZone defaultTimeZone];
    
    
 //   NSLog(@"User requested a %@ on %@ @ %@ for %i minutes",type,day,scheduleDate,minutes);

    FocusCheckSession * entry = [NSEntityDescription insertNewObjectForEntityForName:@"FocusCheckSession" inManagedObjectContext:moCtx];

    entry.firstDate = [scheduleDate timeIntervalSince1970];
    entry.nextDate = entry.firstDate;
    
    // This used to set once, daily, weekly. Now it's the actual days
    /*switch (self.recurrenceChooser.selectedSegmentIndex){
        case 0:
            entry.recurring = 0;
            break;
        case 1:
            entry.recurring = NSDayCalendarUnit;
            break;
        case 2:
            entry.recurring = NSWeekCalendarUnit;
            break;
            
    }*/
    // segments are M,T,W,Th,F,S,Su
    //entry.recurring = ! (self.recurrenceChooser.selectedSegmentIndex == 0);
    
    entry.recurring = 0;
    
    if ( self.recurringSwitch.on) {
        NSIndexSet *selectedDays = self.recurrenceChooser.selectedSegmentIndexes;
        [selectedDays enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            entry.recurring |= (1UL << (idx + 1)); // 1 = Sunday, idx starts at 0
        }];
    }
    
    // Every week for each of the days
    if (entry.recurring != 0) {
        entry.recurring |= NSWeekdayCalendarUnit;
    }
    
    entry.length = minutes;
    entry.sessionType = @"Scheduled Session";
 //   entry.sessionType = type;
    
    NSError * error;
    if (! [moCtx save:&error]) {
        NSLog(@"Unable to save %@",error);
    }
    
    ((UIButton*)sender).selected = false;
    
    [JFCLocalNotificationSupport scheduleNotification:entry];
    if ([self presentingViewController]){
        [[self presentingViewController] dismissViewControllerAnimated:YES completion:^{
            //
        }];
    }
     
}

-(void) setManagedObjectContext:(NSManagedObjectContext *)_moCtx
{
    moCtx = _moCtx;
}

#pragma mark - UIPickerViewDataSource

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if ([pickerView isEqual:self.intervalPicker]) {
    switch (component){
        case kComponentType:
            return [self.typeOptions count];
            break;
        case kComponentLength:
            return [self.lengthOptions count];
            break;
        case kComponentDay:
            return [self.dayOptions count];
            break;
        case 2:
            return [self.minuteOptions count];
            break;
    }
    } else {
        return component == kComponentHours ? [self.hourOptions count] : [self.minuteOptions count];
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if ([pickerView isEqual:self.intervalPicker]) {
        return 1;
    } else {
        return 2;
    }
}

/*
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    CGFloat totalWidth = pickerView.bounds.size.width;
    switch (component){
        case kCom
        case kComponentType:
            return 180.0;
        case kComponentLength:
            return 320.0 - 180.0;
    }
    return 0;
}*/

#pragma mark - UIPickerViewDelegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
  /*  if (component == 0 && [[self.dayOptions objectAtIndex:row] isEqual:@"Now"]){
        [self.datePicker setDate:[NSDate date] animated:YES];
         [self.datePicker setNeedsDisplay];
    } else {
      //  [self.datePicker setEnabled:TRUE];
    }
   */
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual:self.intervalPicker]) {
    switch (component){
        case kComponentType:
            return [self.typeOptions objectAtIndex:row];
            break;
        case kComponentLength:
            return [[self.lengthOptions objectAtIndex:row] stringByAppendingString:@" min"];
            break;
        case kComponentDay:
            return [self.dayOptions objectAtIndex:row];
            break;
        case 2:
            return [self.minuteOptions objectAtIndex:row];
            break;
    }
    } else { // time picker
        switch (component){
            case kComponentHours:
                return [[self.hourOptions objectAtIndex:row] stringByAppendingString:@" hours"];
                break;
            case kComponentMinutes:
                return [[self.minuteOptions objectAtIndex:row] stringByAppendingString:@" minutes"];
                break;
        }
        
    }
    return @"!INVALID!";
}
@end
