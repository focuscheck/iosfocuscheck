//
//  JFCStatistic.m
//  FocusCheck
//
//  Created by Josh Dewald on 7/24/12.
//
//

#import "JFCStatistic.h"

@interface JFCStatistic ()
{
    NSInteger count;
    double total;
}
@end

@implementation JFCStatistic
@synthesize label = _label;

-(id)initWithLabel:(NSString *)label
{
    self = [super init];
    if (self){
        _label = label;
    }
    return self;
}
-(void)addValue:(NSNumber *)value
{
    count++;
    total += [value doubleValue];
    
}

-(NSNumber*)meanAverage
{
    return [NSNumber numberWithDouble:(total / (double)count)];
}
@end
