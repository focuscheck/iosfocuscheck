//
//  FocusCheckEntry.m
//  FocusCheck
//
//  Created by Joshua DeWald on 9/7/12.
//
//

#import "FocusCheckEntry.h"
#import "FocusCheckSession.h"


@implementation FocusCheckEntry

@dynamic entryDate;
@dynamic focusValue;
@dynamic sentToServer;
@dynamic session;

@end
