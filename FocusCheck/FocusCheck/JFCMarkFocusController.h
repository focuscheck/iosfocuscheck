//
//  JFCFirstViewController.h
//  FocusCheck
//
//  Created by Joshua DeWald on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FocusCheckSession.h"
#import "JFCProgressView.h"
#import "JFCBaseViewController.h"

@interface JFCMarkFocusController : JFCBaseViewController
{
    NSManagedObjectContext  *moCtx;
}

@property JFCProgressView   IBOutlet    *intervalProgress;
@property JFCProgressView   IBOutlet    *sessionProgress;
@property UIView            IBOutlet    *questionWrapper;
@property UIView            IBOutlet    *timeToFocusWrapper;
@property UILabel           IBOutlet    *sessionTypeLabel;
@property UIView            IBOutlet    *viewWrapper;
@property UIView            IBOutlet    *doneView;


-(IBAction)focusButtonPressed:(id)sender;
-(IBAction)endSessionButtonPressed:(id)sender;
-(void) setManagedObjectContext:(NSManagedObjectContext *)moCtx;
-(void) setFocusCheckSession:(FocusCheckSession *)session;
-(void) requestFocus;
@end
